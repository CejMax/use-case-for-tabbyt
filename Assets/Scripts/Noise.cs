using UnityEngine; 
using System.Collections;
using System.Collections.Generic;

public static class Noise {
  private static RandomNumberGenerator rngNoise = new RandomNumberGenerator(0);
  private static RandomNumberGenerator rngObject = new RandomNumberGenerator(0);

  private const int INT = 0;
  private const int FLOAT = 1;

  private const int FORESTINDEX = 0;

  private static int octaves = 8;
  private static float scale = 48.68f;
  private static float persistance = 0.5f;
  private static float lacunarity = 2f;

  public static float meshHeightMultiplier { get; private set; } = 31.1f;
  public static int mapWidth { get; private set; } = 241;
  public static int mapHeight { get; private set; } = 241;

  public static int climate{ get; private set; } = 0;

  private static List<VariableData> mapVariablesData = new List<VariableData>(){
    new VariableData("climate", INT, 0, 2)
  };

  private static List<VariableData> forestVariablesData = new List<VariableData>(){
    new VariableData("objPrefabIndex", INT, 0, 3)
  };


  private static List<ObjectData> objectDataList = new List<ObjectData>(){
    new ObjectData("Forest", -1, 6f, 0.37f, 0.6f, 8, forestVariablesData)
  };

  /// <summary>
  /// Returns the generated heightmap for the map at the specified offset.
  /// </summary>
  public static float[,] GenerateNoiseMap(Vector2 offset) {

    //Resetting random seed for consistent outputs
    rngNoise.ResetRng();

    float[,] noiseMap = new float[mapWidth,mapHeight];
    Vector2[] octaveOffsets = new Vector2[octaves];

    float maxPossibleHeight = 0;
    float amplitude = 1;
    float frequency = 1;

    // Calculate each octave offset and the maximum possible height achievable with this map
    for (int i = 0; i < octaves; i++) {
      float offsetX = rngNoise.RandomRangeI (-100000, 100000) + offset.x;
      float offsetY = rngNoise.RandomRangeI (-100000, 100000) + offset.y;
      octaveOffsets [i] = new Vector2 (offsetX, offsetY);

      maxPossibleHeight += amplitude;
      amplitude *= persistance;
    }

    float halfWidth = mapWidth / 2f;
    float halfHeight = mapHeight / 2f;

    for (int y = 0; y < mapHeight; y++) {
      for (int x = 0; x < mapWidth; x++) {
        amplitude = 1;
        frequency = 1;
        float noiseHeight = 0;

        for (int i = 0; i < octaves; i++) {
          float sampleX = (x-halfWidth + octaveOffsets[i].x) / scale * frequency ;
          float sampleY = (y-halfHeight + octaveOffsets[i].y) / scale * frequency ;

          float perlinValue = Mathf.PerlinNoise (sampleX, sampleY) * 2 - 1;
          noiseHeight += perlinValue * amplitude;
          amplitude *= persistance;
          frequency *= lacunarity;
        }
        float normalizedHeight = (noiseHeight + 1) / (maxPossibleHeight/0.9f);
        noiseMap [x, y] = Mathf.Clamp(normalizedHeight,0, int.MaxValue);
      }
    }
    return noiseMap;
  }

  /// <summary>
  /// Generates random values for each map variable.
  /// </summary>
  private static void GenerateMapValues() {
    climate = rngObject.RandomRangeI((int) mapVariablesData[0].begin, (int)mapVariablesData[0].end);
  }

  /// <summary>
  /// Returns a List of the specified generated objects for the specified heightmap and offset.
  /// </summary>
  public static MapObject[] GenerateObjects(float[,] heightMap, Vector2 offset, string nameObject) {
    Vector2[] points;
    rngObject.CalculateSeed(offset.x, offset.y);
    switch(nameObject) {
     case "Forest":
        points = GeneratePoints(FORESTINDEX);
        List<MapObject> forestList = GenerateForestValues(points, FORESTINDEX);
        forestList = FilterObjects(heightMap, forestList, FORESTINDEX);
        return forestList.ToArray();
      default:
        return null;
    }
  }

  /// <summary>
  /// Returns a List of generated Forest objects with random values defined by objectDataList.
  /// </summary>
  private static List<MapObject> GenerateForestValues (Vector2[] points, int objectDataIndex) {
    List<ForestObject> newForest = new List<ForestObject>();
    int objPrefabIndex;
    ObjectData dataFromObject = objectDataList[objectDataIndex];
    foreach ( Vector2 point in points ){
      objPrefabIndex = rngObject.RandomRangeI((int)dataFromObject.variables[0].begin, (int)dataFromObject.variables[0].end);
      newForest.Add(new ForestObject(objPrefabIndex , point));
    }
    return new List<MapObject>(newForest.ToArray());
  }

  /// <summary>
  /// Returns the specified List of objects after having been filted by their position in the heightmap defined by objectDataList.
  /// </summary>
  private static List<MapObject> FilterObjects(float[,] heightMap, List<MapObject> objects, int objectDataIndex) {
    int indexX;
    int indexY;
    int i = 0;
    ObjectData dataFromObject = objectDataList[objectDataIndex];
    while(i < objects.Count) {
      indexX = (int) Mathf.Floor(objects[i].GetPositionX());
      indexY = (int) Mathf.Floor(objects[i].GetPositionY());
      if (!(heightMap[indexX, indexY] >= dataFromObject.minHeight && heightMap[indexX, indexY] <= dataFromObject.maxHeight)){
        objects.RemoveAt(i);
      } else {
        i++;
      }
    }

    if (dataFromObject.maxAmount >= 0) {
      i = objects.Count;
      int index;
      while (i > dataFromObject.maxAmount) {
        index = rngObject.RandomRangeI(0, 10000) % objects.Count;
        objects.RemoveAt(index);
        i = objects.Count;
      }
    }

    return objects;
  }

  /// <summary>
  /// Returns the coordinates of all the objects to be placed in the map defined by objectDataList.
  /// </summary>
  private static Vector2[] GeneratePoints(int objectDataIndex) {
    int numSamplesBeforeRejection = objectDataList[objectDataIndex].numSamplesBeforeRejection;
    float radius = objectDataList[objectDataIndex].radius;

    float cellSize = radius/Mathf.Sqrt(2);
    int[,] grid = new int[Mathf.CeilToInt(mapWidth/cellSize), Mathf.CeilToInt(mapHeight/cellSize)];
    List<Vector2> points = new List<Vector2>();
    List<Vector2> spawnPoints = new List<Vector2>();

    spawnPoints.Add(new Vector2(mapWidth/2, mapHeight/2));

    while (spawnPoints.Count > 0) {
      int spawnIndex = rngObject.RandomRangeI(0,spawnPoints.Count);
      Vector2 spawnCentre = spawnPoints[spawnIndex];
      bool candidateAccepted = false;
      for (int i = 0; i < numSamplesBeforeRejection; i++) {
        float angle = rngObject.RandomNext() * Mathf.PI * 2;
        Vector2 dir = new Vector2(Mathf.Sin(angle), Mathf.Cos(angle));
        Vector2 candidate = spawnCentre + dir * rngObject.RandomRangeF(radius, 2*radius);
        if (IsValid(candidate, cellSize, radius, points, grid)) {
          points.Add(candidate);
          spawnPoints.Add(candidate);
          grid[(int)(candidate.x/cellSize), (int)(candidate.y/cellSize)] = points.Count;
          candidateAccepted = true;
          break;
        }
      }
      if(!candidateAccepted) {
        spawnPoints.RemoveAt(spawnIndex);
      }
    }

    return points.ToArray();
  }

  /// <summary>
  /// Returns a boolean whether the candidate is a valid coordinate for an object to be placed.
  /// </summary>
  static bool IsValid(Vector2 candidate, float cellSize, float radius, List<Vector2> points, int[,] grid) {
    if (candidate.x >=0 && candidate.x < mapWidth && candidate.y >= 0 && candidate.y < mapHeight) {
      int cellX = (int)(candidate.x/cellSize);
      int cellY = (int)(candidate.y/cellSize);
      int searchStartX = Mathf.Max(0,cellX -2);
      int searchEndX = Mathf.Min(cellX+2,grid.GetLength(0)-1);
      int searchStartY = Mathf.Max(0,cellY -2);
      int searchEndY = Mathf.Min(cellY+2,grid.GetLength(1)-1);

      for (int x = searchStartX; x <= searchEndX; x++) {
        for (int y = searchStartY; y <= searchEndY; y++) {
          int pointIndex = grid[x,y]-1;
          if (pointIndex != -1) {
            float sqrDst = (candidate - points[pointIndex]).sqrMagnitude;
            if (sqrDst < radius*radius) {
              return false;
            }
          }
        }
      }
      return true;
    }
    return false;
  }

  /// <summary>
  /// Initializes the relevant map data with the specified seed.
  /// </summary>
  public static void SetMapSeed(int seed){
    rngNoise.SetSeed(seed);
    SetRangeSeed(seed);
    GenerateMapValues();
    SetRangeSeed(seed);
  }

  /// <summary>
  /// Reinitializes the object related RNG with the specified seed.
  /// </summary>
  public static void SetRangeSeed(int seed){
    rngObject.SetSeed(seed);
  }
}

public struct ObjectData {
  public readonly string name;
  public readonly int maxAmount;
  public readonly float radius;
  public readonly float minHeight;
  public readonly float maxHeight;
  public readonly int numSamplesBeforeRejection;
  public List<VariableData> variables;

  public ObjectData (string name, int maxAmount, float radius, float minHeight, float maxHeight,
                     int numSamplesBeforeRejection, List<VariableData> variables) {
    this.name = name;
    this.maxAmount = maxAmount;
    this.radius = radius;
    this.minHeight = minHeight;
    this.maxHeight = maxHeight;
    this.variables = variables;
    this.numSamplesBeforeRejection = numSamplesBeforeRejection;
  }
}

public struct VariableData {
  public readonly string name;
  public readonly int type;
  public readonly float begin;
  public readonly float end;

  public VariableData (string name, int type, float begin, float end) {
    this.name = name;
    this.type = type;
    this.begin = begin;
    this.end = end;
  }
}

