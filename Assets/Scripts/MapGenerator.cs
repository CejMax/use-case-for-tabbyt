﻿using UnityEngine;
using System.Collections;

public class MapGenerator : MonoBehaviour {

  public enum DrawMode {NoiseMap, ColourMap, Mesh, MeshObjects};
  public DrawMode drawMode;

  private int mapChunkWidth;
  private int mapChunkHeight;

  private MapDisplay display;
  
  const int levelOfDetail = 0;

  private float meshHeightMultiplier;

  public int seed;
  public Vector2 offset; 

  public AnimationCurve meshHeightCurve;

  public bool autoUpdate;

  public TerrainList[] climates;

  private int climateInUse;

  /// <summary>
  /// Returns the MeshData corresponding to the specified parameters.
  /// </summary>
  public void GenerateMap() {
    if(display == null){
      Awake();
    }
    this.transform.position = new Vector3(0,0,0);
    this.transform.localScale = Vector3.one;
    
    Noise.SetMapSeed(seed);
    climateInUse = Noise.climate;
    meshHeightMultiplier = Noise.meshHeightMultiplier;
    mapChunkWidth = Noise.mapWidth;
    mapChunkHeight = Noise.mapHeight;

    display.EraseForestList();

    if (drawMode == DrawMode.NoiseMap) {
      MapData mapData = GenerateMapData (offset);
      display.DrawTexture (TextureGenerator.TextureFromHeightMap (mapData.heightMap));
      display.textureRender.enabled = true;
      display.meshRenderer.enabled = false;

      } else if (drawMode == DrawMode.ColourMap) {
      MapData mapData = GenerateMapData (offset);
      display.DrawTexture (TextureGenerator.TextureFromColourMap (mapData.colourMap, mapChunkWidth, mapChunkHeight));
      display.textureRender.enabled = true;
      display.meshRenderer.enabled = false;

    } else if (drawMode == DrawMode.Mesh) {
      MapData mapData = GenerateMapData (offset);
      display.DrawMesh (MeshGenerator.GenerateTerrainMesh (
                          mapData.heightMap, meshHeightMultiplier, meshHeightCurve, levelOfDetail),
                          TextureGenerator.TextureFromColourMap (mapData.colourMap, mapChunkWidth, mapChunkHeight),
                          mapData.heightMap.GetLength (0));
      display.textureRender.enabled = false;
      display.meshRenderer.enabled = true;

    } else if (drawMode == DrawMode.MeshObjects) {
      MapData mapData = GenerateRestrictionMapData (offset);
      display.DrawMesh (MeshGenerator.GenerateTerrainMesh (
                          mapData.heightMap, meshHeightMultiplier, meshHeightCurve, levelOfDetail),
                          TextureGenerator.TextureFromColourMap (mapData.colourMap, mapChunkWidth, mapChunkHeight),
                          mapData.heightMap.GetLength (0));
      display.DrawForest(mapData.forestMap, new Vector2(0,0), this.transform, CurrentChunk(offset));
      display.textureRender.enabled = false;
      display.meshRenderer.enabled = true;
      
    }
  }

  /// <summary>
  /// Returns the MeshData corresponding to the specified parameters.
  /// </summary>
  MapData GenerateMapData(Vector2 centre) {
    float[,] noiseMap = Noise.GenerateNoiseMap (centre);

    MapObject[] forestMap = null;
    Color[] colourMap = new Color[mapChunkWidth * mapChunkHeight];
    for (int y = 0; y < mapChunkWidth; y++) {
      for (int x = 0; x < mapChunkHeight; x++) {
        float currentHeight = noiseMap [x, y];
        for (int i = 0; i < climates[climateInUse].regions.Length; i++) {
          if (currentHeight >= climates[climateInUse].regions [i].height) {
            colourMap [y * mapChunkWidth + x] = climates[climateInUse].regions [i].colour;
          } else {  
            break;
          }
        }
      }
    }
    return new MapData(noiseMap, colourMap, forestMap);
  }

  /// <summary>
  /// Returns the MeshData corresponding to the specified parameters.
  /// </summary>
  MapData GenerateRestrictionMapData(Vector2 centre) {
    float[,] noiseMap = Noise.GenerateNoiseMap (centre);

    Vector2 chunkCoord = CurrentChunk(centre);
    MapObject[] forestMap = Noise.GenerateObjects(noiseMap, chunkCoord, "Forest");

    Color[] colourMap = new Color[mapChunkWidth * mapChunkHeight];
    for (int y = 0; y < mapChunkWidth; y++) {
      for (int x = 0; x < mapChunkHeight; x++) {
        float currentHeight = noiseMap [x, y];
        for (int i = 0; i < climates[climateInUse].regions.Length; i++) {
          if (currentHeight >= climates[climateInUse].regions [i].height) {
            colourMap [y * mapChunkWidth + x] = climates[climateInUse].regions [i].colour;
          } else {  
            break;
          }
        }
      }
    }
    return new MapData(noiseMap, colourMap, forestMap);
  }

  /// <summary>
  /// Erases all forest objects.
  /// </summary>
  public void RemoveForest(){
    if(display == null){
      Awake();
    }
    display.EraseForestList();
  }

  /// <summary>
  /// Returns the chuck coordinate corresponding to the specified coordinates.
  /// </summary>
  private Vector2 CurrentChunk(Vector2 centre) {
    return new Vector2(Mathf.RoundToInt(centre.x / mapChunkWidth), Mathf.RoundToInt(centre.y / mapChunkHeight));
  }

  void Awake() {
    display = FindObjectOfType<MapDisplay> ();
  }

  void OnValidate() {

  }
}

[System.Serializable]
public struct TerrainType {
  public string name;
  public float height;
  public Color colour;
}

[System.Serializable]
public struct TerrainList {
  public string name;
  public TerrainType[] regions;
}

public struct MapData {
  public readonly float[,] heightMap;
  public readonly Color[] colourMap;
  public readonly MapObject[] forestMap;

  public MapData (float[,] heightMap, Color[] colourMap, MapObject[] forestMap) {
    this.heightMap = heightMap;
    this.colourMap = colourMap;
    this.forestMap = forestMap;
  }
}