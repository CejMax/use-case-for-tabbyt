﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MapDisplay : MonoBehaviour {

	public Renderer textureRender;
	public MeshFilter meshFilter;
	public MeshRenderer meshRenderer;
	public Element[] forest;

	public float objectScale;

	private Vector3 mapScale;
	private List<GameObject> forestList = new List<GameObject>();
  private int vectorsPerLine;

  /// <summary>
  /// Draws the specified Texture.
  /// </summary>
	public void DrawTexture(Texture2D texture) {
		textureRender.sharedMaterial.mainTexture = texture;
		textureRender.transform.localScale = new Vector3 (-texture.width, 1, texture.height);
	}

  /// <summary>
  /// Draws the specified Mesh.
  /// </summary>
	public void DrawMesh(MeshData meshData, Texture2D texture, int vectPerLine) {
    mapScale = meshFilter.transform.localScale;
		meshFilter.sharedMesh = meshData.CreateMesh ();
		meshRenderer.sharedMaterial.mainTexture = texture;
    vectorsPerLine = vectPerLine;
	}

  /// <summary>
  /// Instantiates all specified ForestObjects on the map.
  /// </summary>
	public void DrawForest(MapObject[] forestData, Vector2 centre, Transform parent, Vector2 chunk) {
		Vector3 treePosition;
		Vector3 vertexAtIndex;
		GameObject newElement;
		GameObject randomPrefab;
		
		int meshIndex;
		int meshIndexWidth;
		int meshIndexHeight;

		float prefabOffset;

		Vector3[] mapVertices = meshFilter.sharedMesh.vertices;

    // moves the generation of the forest to coincide with the centre of the chunk
		float offset = (vectorsPerLine - 1) / -2f; 

    int treeX = 0;
    int treeY = 0;
    float treeZMirror = 0;

		foreach (MapObject tree in forestData) {

      treeX = (int) Mathf.Floor(tree.GetPositionX());
      treeY = (int) Mathf.Floor(tree.GetPositionY());

      meshIndexWidth = treeX;
      meshIndexHeight = treeY * vectorsPerLine;
			meshIndex = meshIndexWidth + meshIndexHeight;
			vertexAtIndex = mapVertices[meshIndex];

      ForestObject auxTree = (ForestObject) tree;
			randomPrefab = forest[0].prefab[auxTree.objPrefabIndex];
			prefabOffset = forest[0].ObjectHeightOffset(randomPrefab);
			
			newElement = Instantiate(randomPrefab, parent);

      // mirror position with respect to the centre of the chunk on the X axis
      if (tree.GetPositionY() < 0) {
        treeZMirror = Mathf.Abs(tree.GetPositionY() - centre[1]) + centre[1]; 
      }
      else{
        treeZMirror = centre[1] - Mathf.Abs(tree.GetPositionY() - centre[1]);
      }
        
			treePosition = new Vector3(
                          offset + tree.GetPositionX(), 
                          vertexAtIndex[1] + prefabOffset, 
                          treeZMirror - offset);

			newElement.transform.position = treePosition;
			newElement.transform.localScale = Vector3.one * objectScale;
			forestList.Add(newElement);
		}
		parent.transform.localScale = mapScale;
	}

  /// <summary>
  /// Eliminates all GameObjects in the forestlist.
  /// </summary>
	public void EraseForestList(){
		foreach (GameObject tree in forestList) {
			DestroyImmediate(tree);
		}
    forestList.Clear();
	}

	void OnValidate() {
		if (objectScale < 0.01)
			objectScale = 1;
	}

}

[System.Serializable]
public class Element {

  public string name;

  public GameObject[] prefab;
  public float[] prefabHeightAdjustment;

  /// <summary>
  /// Assures that each prefab has a coresponding height adjustment variable initialized.
  /// </summary>
  public void DefaultInit () {
  	if (prefab.Length > 0 && prefab.Length != prefabHeightAdjustment.Length) {
  		prefabHeightAdjustment = new float[prefab.Length];
  		for (int i = 0; i < prefabHeightAdjustment.Length; i++) {
  			prefabHeightAdjustment[i] = 0;
  		}
  	}
  }

  /// <summary>
  /// Returns the height adjustment coresponding to the specified GameObject.
  /// </summary>
  public float ObjectHeightOffset(GameObject treeObj) {	
  	float heightOffset = 0;
  	int index = 0;
  	foreach (GameObject tree in prefab) {
  		if (GameObject.ReferenceEquals (treeObj, tree)) {
  			heightOffset = prefabHeightAdjustment[index];
  		}
  		index++;
  	}
  	return heightOffset;
  }
}
